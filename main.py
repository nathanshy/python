from classes.game import Person, BColors
from classes.magic import Spell

#Create Black Magic
fire = Spell("Fire", 10, 60, "black")
thunder = Spell("Thunder", 20, 150, "black")
blizzard = Spell("Blizzard", 40, 200, "black")
meteor = Spell("Meteor", 50, 300, "black")
quake = Spell("Quake", 70, 500, "black")

#Create White Magic
cure = Spell("Cure", 12, 120, "white")
cura = Spell("Cura", 18, 200, "white")

player = Person(450, 65, 60, 34, [fire, thunder, blizzard, meteor, cure, cura])
enemy = Person (1200, 65, 45, 25, [])

running = True

i = 0

print(BColors.FAIL + BColors.BOLD + "AN ENEMY ATTACKS!" + BColors.ENDC)

while running:
    print("=====================================")
    player.choose_action()
    
    choice = input("Choose action: ")
    index = int(choice) - 1
    print("You Chose:", player.actions[index])

    #regular attack
    if index == 0:
        dmg = player.generate_damage()
        enemy.take_damage(dmg)
        print(BColors.BOLD, "You attacked for:", BColors.UNDERLINE, dmg, BColors.ENDC)
        print(BColors.BOLD + "Enemy HP: " + str(enemy.get_hp()) + "/" + str(player.get_max_hp()) + BColors.ENDC)

    #magic
    if index == 1:
        player.choose_magic()
        spellIndex = int(input("Choose Spell:")) - 1

        spell = player.magic[spellIndex]
        magic_dmg = spell.generate_damage()

        if player.get_mp() > (spell.cost):

            print("You cast", BColors.BOLD,spell.name, BColors.ENDC)


            if spell.spell_type == "white":
                print(BColors.BOLD, "Your spell healed for:", BColors.UNDERLINE, magic_dmg, BColors.ENDC)
                player.heal(magic_dmg)
                print(BColors.BOLD, "Player HP: " + str(player.get_hp())+ "/"+ str(player.get_max_hp()), BColors.ENDC)
                print(BColors.BOLD + "Remaining MP: " + BColors.OKBLUE + str(player.get_mp()) + "/" + str(player.get_max_mp()), BColors.ENDC)


            if spell.spell_type == "black":
                print(BColors.BOLD, "Your spell hit for:", BColors.UNDERLINE, magic_dmg, BColors.ENDC)
                enemy.take_damage(magic_dmg)
                print(BColors.BOLD, "Enemy HP: " + str(enemy.get_hp())+ "/"+ str(enemy.get_max_hp()), BColors.ENDC)
                print(BColors.BOLD + "Remaining MP: " + BColors.OKBLUE + str(player.get_mp()) + "/" + str(player.get_max_mp()), BColors.ENDC)


        else:
            print(BColors.BOLD, BColors.FAIL,"Not enough MP for that move", BColors.ENDC)
            continue


    if enemy.get_hp() == 0:
        print(BColors.WARNING, BColors.BOLD, "You have killed the enemy", BColors.ENDC)
        running = False
        break

    
    #enemy attacks player
    #random whether magic or action
        #random spell if magictake_damage
    enemy_choice = 1
    enemy_dmg = enemy.generate_damage()
    player.take_damage(enemy_dmg)
    print(BColors.FAIL, "Enemy Attacked for", BColors.UNDERLINE, enemy_dmg, BColors.ENDC)
    print(BColors.BOLD + "Remaining HP: " + BColors.OKGREEN + str(player.get_hp()) + "/" + str(player.get_max_hp()) + BColors.ENDC)

    if player.get_hp() == 0:
        print(BColors.FAIL + BColors.BOLD +"You have died..." + BColors.ENDC)
        running = False
        break


    #running = False